﻿using DefaultAPI.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DefaultAPI.Controllers
{

    public class HomeController : Controller
    {
        public readonly UserManager<ApplicationUser> _manager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public HomeController( UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _manager = userManager;
            _signInManager = signInManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        
        public IActionResult Login()
        {
            return View();
        }
        

        [HttpPost]
        public async Task<ActionResult> Login(string userName, string password )
        {
            var findUser = await _manager.FindByNameAsync(userName);

            if (findUser != null)
            {
               var signinResult = await _signInManager.PasswordSignInAsync(findUser, password, false, false);
            }

            return RedirectToAction("ProfileData");
            //return RedirectToAction("Index");

        }

        
        public IActionResult Register()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Register(string userName, string password, string email , DateTime date)
        {
            var user = new ApplicationUser()
            {
                UserName = userName,
                Email = email,
                DateOfBirth = date,
                
            };
            var result = await _manager.CreateAsync(user, password);
            var result2 = await _manager.AddClaimAsync(user, new Claim(ClaimTypes.DateOfBirth , Convert.ToString(date)));
            if (result.Succeeded && result2.Succeeded)
            {
                var signinResult = await _signInManager.PasswordSignInAsync(user.UserName, password, false, false);
            }
            return RedirectToAction("Index");
        }
        public async Task<ActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index");
        }

        [Authorize (Policy = "DataofBirth")]
        public ActionResult ProfileData()
        {
            return View();
        }


        public ActionResult GoogleSignIn()
        {
            /*
            return new ChallengeResult(
            GoogleDefaults.AuthenticationScheme,
             new AuthenticationProperties
             {
                  RedirectUri = Url.Action(nameof(LoginCallback))
             });
            */
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> LoginCallback()
        {
            /*
            var authenticateResult = await HttpContext.AuthenticateAsync("External");           
            
            if (!authenticateResult.Succeeded)
                return BadRequest(); 

            var claimsIdentity = new ClaimsIdentity("Cookies");

            claimsIdentity.AddClaim(authenticateResult.Principal.FindFirst(ClaimTypes.NameIdentifier));
            claimsIdentity.AddClaim(authenticateResult.Principal.FindFirst(ClaimTypes.Email));

            await HttpContext.SignInAsync(
                "Cookies",
                new ClaimsPrincipal(claimsIdentity));
            /*
            new AuthenticationProperties
            {
                IsPersistent = true
            }); */
            return RedirectToAction("Index");
        }

    }
}
