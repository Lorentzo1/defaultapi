﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DefaultAPI.Controllers
{
    [Route ("data")]
    [ApiController]
    public class IdentityController : ControllerBase
    {
        [Route("test")]
        public string GettingData()
        {
            return "Api test ok";
        }
    }
}
