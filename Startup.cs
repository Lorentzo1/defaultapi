using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DefaultAPI.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using static DefaultAPI.Data.CostumeRequirements;

namespace DefaultAPI
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            using (var client = new SqliteDbContext())
            {
                client.Database.EnsureCreated();
            }
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddMvc();
            services.AddEntityFrameworkSqlite().AddDbContext<SqliteDbContext>();
            services.AddIdentity <ApplicationUser, IdentityRole>
                (opt =>
                {
                    opt.Password.RequireDigit = false;
                    opt.Password.RequiredLength = 3;
                    opt.Password.RequireLowercase = false;
                    opt.Password.RequireNonAlphanumeric = false;
                    opt.Password.RequireDigit = false;
                    opt.Password.RequireUppercase = false;
                }
                )
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<SqliteDbContext>();

            services.ConfigureApplicationCookie(opt =>
             {
               opt.Cookie.Name = "IdentityCookie";                

             });


            /*
            services.AddAuthentication("Cookie")
                .AddCookie("Cookie", opt =>
                {
                    opt.Cookie.Name = "Google Cookie";

                })
                .AddGoogle(opt =>
                {

                    opt.ClientId = Configuration["AuthenticationGoogle:Google:ClientId"];
                    opt.ClientSecret = Configuration["AuthenticationGoogle:Google:ClientSecret"];

                });
            */
            

            services.AddAuthorization(opt => 
            {
                opt.AddPolicy("DataofBirth", policyBuilder =>
                {
                    policyBuilder.AddRequirements(new CostumeRequirements(ClaimTypes.DateOfBirth));
                });
            });

            services.AddScoped<IAuthorizationHandler, CostumeRequirementHandler>();
            services.AddScoped<IAppRepository, OrderRepository>();




        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                /*endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{cotroller=Home}/{action=Index}"
                    );*/
                endpoints.MapControllers();
            });
        }
    }
}
