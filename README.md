This small app demonstrates various fields of .Net Core App a developer needs to solve do deploy a functioning solution.

The project includes - MVC -home controller- with a basic Razor view for Registering, Logingin and showing Authorized page - thus shows how to solve authentication (cookie) and authorization with costume policies.

- The app works with SQLite for being lightweight and having the ability to be deployed locally. The database is configured to work with Build-in .net core identity,
- Identity is edited to AplicationUser to add the date of birth field into an authentication model for higher security
- Application User is connected into theoretical "Order" model (that can be extended) with one-to-many relation
- The app is ready to utilize google OAuth third party cookies for logins, but is not finished due to limitation of Google-API
- Kestrel is ready for deployment to listen on HTTPS ports waiting for the certificate to be supplied.
- The app has also unfinished REST/API for managing orders, Interface and Repository are completed and added into services.
- and much more
