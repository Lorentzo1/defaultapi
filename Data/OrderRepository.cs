﻿using DefaultAPI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DefaultAPI.Data
{
    public class OrderRepository : IAppRepository
    {
        private readonly SqliteDbContext _context;

        public OrderRepository()
        {
        }

        public OrderRepository(SqliteDbContext context)
        {
            _context = context;
        }

        public void CreateBid(Order bid)
        {
            if (bid == null)
            {
                throw new ArgumentException(nameof(bid));
            }
            _context.Add(bid);

        }

        public void DeleteBid(Order bid)
        {
            if (bid == null)
            {
                throw new ArgumentException(nameof(bid));
            }
            _context.Orders.Remove(bid);
        }

        public IEnumerable<Order> GetBidsData()
        {
            return _context.Orders.ToList();
        }

        public Order GetBidsDataId(int id)
        {
            return _context.Orders.FirstOrDefault(x => x.ID == id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateBis(Order bid)
        {
            //Nothing
        }
    }
}
