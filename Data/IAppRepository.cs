﻿using DefaultAPI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DefaultAPI.Data
{
    public interface IAppRepository
    {

            IEnumerable<Order> GetBidsData();

            Order GetBidsDataId(int id);

            void CreateBid(Order bid);

            bool SaveChanges();

            void UpdateBis(Order bid);

            void DeleteBid(Order bid);
    }
}
