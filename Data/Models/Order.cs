﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DefaultAPI.Data.Models
{
    public class Order
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength (40)]
        public string Name { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime ExpirationDate { get; set; }


        public int Price { get; set; }

        public ApplicationUser applicationUser { get; set; }

    }
}
