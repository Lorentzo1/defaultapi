﻿using DefaultAPI.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DefaultAPI.Data
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            OrdersCollection = new HashSet<Order>();
        }

        public DateTime DateOfBirth { get; set; }



        public virtual ICollection<Order> OrdersCollection { get; set; }
    }
}
