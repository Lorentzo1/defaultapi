﻿using DefaultAPI.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DefaultAPI.Data
{
    public class SqliteDbContext : IdentityDbContext<ApplicationUser>
    {

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlite("Filename=IdentityDataBase.db");
        }



        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
                builder.Entity<Order>().ToTable("Order");

            builder.Entity<Order>()
                .HasOne(a => a.applicationUser)
                .WithMany(aa => aa.OrdersCollection);
                
                
        }
        
    }
}
