﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DefaultAPI.Data
{
    public class CostumeRequirements : IAuthorizationRequirement
    {
        public CostumeRequirements( string claimType)
        {
            _claimType = claimType;
        }

        public string _claimType { get; }

        public class CostumeRequirementHandler : AuthorizationHandler<CostumeRequirements>
        {
            protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CostumeRequirements requirement)
            {

                var claim = context.User.Claims.Any(x => x.Type == requirement._claimType);
                if (claim)
                {
                    context.Succeed(requirement);
                }

                return Task.CompletedTask;
            }
        }
    }
}
